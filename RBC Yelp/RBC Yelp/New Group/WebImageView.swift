//
//  WebImageView.swift
//  RBC Yelp
//
//  Created by Peter Rutherford on 2018-03-10.
//  Copyright © 2018 Peter Rutherford. All rights reserved.
//

import Foundation
import UIKit

// Class used to extend UIImageView to allow easy loading/caching of server images
class WebImageView: UIImageView
{
	static let imageCache = NSCache<NSString, AnyObject>()

	var loadingImageString: String?

	func loadImageFrom(url: URL)
	{
		self.image = nil
		self.loadingImageString = url.absoluteString

		if let cachedImage = WebImageView.imageCache.object(forKey: url.absoluteString as NSString) as? UIImage
		{
			self.image = cachedImage
			return
		}

		let task = URLSession.shared.dataTask(with: url)
		{ (data, response, error) in

			if data != nil && error == nil
			{
				if let image = UIImage(data: data!)
				{
					WebImageView.imageCache.setObject(image, forKey: url.absoluteString as NSString)

					if self.loadingImageString == url.absoluteString
					{
						DispatchQueue.main.async
						{
							self.image = image
							self.alpha = 0

							UIView.animate(withDuration: 0.2)
							{	self.alpha = 1
							}
						}
					}
				}
			}
		}

		task.resume()
	}
}
