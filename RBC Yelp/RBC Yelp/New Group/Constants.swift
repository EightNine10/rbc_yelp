//
//  Constants.swift
//  RBC Yelp
//
//  Created by Peter Rutherford on 2018-03-10.
//  Copyright © 2018 Peter Rutherford. All rights reserved.
//

import Foundation
import UIKit

// Restaurant Collection View
let GRID_MARGIN: CGFloat = 10
let GRID_CELL_MAX_WIDTH: CGFloat = 190
let DEFAULT_SEARCH_TERM = "Ethiopian"

// Settings View Controller
let SORT_OPTIONS_DISPLAY_ARRAY = ["Best Match", "Rating", "Review Count", "Distance"]
let SORT_OPTIONS_ARRAY = ["best_match", "rating", "review_count", "distance"]
let LIMIT_OPTIONS_ARRAY = ["10", "20", "30"]
let PRICE_OPTIONS_ARRAY = ["$", "$$", "$$$", "$$$$"]

// Yelp API Manager
let YELP_SEARCH_URL = "https://api.yelp.com/v3/businesses/search?location=Canada"
let YELP_BUSINESS_LOOKUP_URL = "https://api.yelp.com/v3/businesses/"
let YELP_API_KEY = "VUfEPTs3VYqvOPTWlErHFrsM4-V0aQ2_fx3zLMXyJ-CeQ4B6pq6YrciRW5KXTE2CwFCMdE66gTXScboHjBfbqpuxdtGWwPrU7QRRlMji8rHSj1t4m9-TH-Ao5QCkWnYx"

// Images
let starImageNames = [	"5_Star_Rating_System_0_stars_T",
						"5_Star_Rating_System_0_and_half_star_T",
						"5_Star_Rating_System_1_star_T",
						"5_Star_Rating_System_1_and_a_half_stars_T",
						"5_Star_Rating_System_2_stars_T",
						"5_Star_Rating_System_2_and_a_half_stars_T",
						"5_Star_Rating_System_3_stars_T",
						"5_Star_Rating_System_3_and_a_half_stars_T",
						"5_Star_Rating_System_4_stars_T",
						"5_Star_Rating_System_4_and_a_half_stars_T",
						"5_Star_Rating_System_5_stars_T",
					 ]
