//
//  UserDataManager.swift
//  RBC Yelp
//
//  Created by Peter Rutherford on 2018-03-10.
//  Copyright © 2018 Peter Rutherford. All rights reserved.
//

import Foundation

class UserDataManager
{
	static var shared = UserDataManager()

	var sortOptionIndex = 0
	var limitOptionIndex = 0

	var priceOptions = [true, true, true, true]

	func getLimitParameter() -> String
	{
		return LIMIT_OPTIONS_ARRAY[limitOptionIndex]
	}

	func getPriceParameter() -> String
	{
		var priceParameter = ""

		for (index, option) in priceOptions.enumerated()
		{
			if option
			{	priceParameter += "\(index + 1),"
			}
		}

		if priceParameter != ""
		{	priceParameter.remove(at: priceParameter.index(before: priceParameter.endIndex))
		}

		return priceParameter
	}
}
