//
//  YelpAPIManager.swift
//  RBC Yelp
//
//  Created by Peter Rutherford on 2018-03-10.
//  Copyright © 2018 Peter Rutherford. All rights reserved.
//

import Foundation

// This class creates the requests to retrieve data from the Yelp Fusion APIs
class YelpAPIManager
{
	class func getSearchResultsFor(term: String, completion: @escaping (JSON?) -> ())
	{
		var searchURLString = YELP_SEARCH_URL
		searchURLString += "&limit=" + UserDataManager.shared.getLimitParameter()
		searchURLString += "&term=" + term
		searchURLString += "&sort_by=" + SORT_OPTIONS_ARRAY[UserDataManager.shared.sortOptionIndex]
		searchURLString += "&price=" + UserDataManager.shared.getPriceParameter()

		searchURLString = searchURLString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? searchURLString

		if let searchURL = URL(string: searchURLString)
		{
			let request = YelpAPIManager.createRequestFrom(url: searchURL)

			let task = URLSession.shared.dataTask(with: request)
			{ (data, response, error) in

				if error != nil || data == nil
				{	completion(nil)
				}
				else
				{
					do
					{	try completion(JSON(data: data!))
					}
					catch
					{	completion(nil)
					}
				}
			}

			task.resume()
		}
		else
		{	completion(nil)
		}
	}

	class func getBusinessLookupFor(businessID: String, completion: @escaping (JSON?) -> ())
	{
		if let lookupURL = URL(string: YELP_BUSINESS_LOOKUP_URL + businessID)
		{
			let request = YelpAPIManager.createRequestFrom(url: lookupURL)

			let task = URLSession.shared.dataTask(with: request)
			{ (data, response, error) in

				if error != nil || data == nil
				{	completion(nil)
				}
				else
				{
					do
					{	try completion(JSON(data: data!))
					}
					catch
					{	completion(nil)
					}
				}
			}

			task.resume()
		}
	}

	class func getReviewsFor(businessID: String, completion: @escaping (JSON?) -> ())
	{
		if let lookupURL = URL(string: YELP_BUSINESS_LOOKUP_URL + businessID + "/reviews")
		{
			let request = YelpAPIManager.createRequestFrom(url: lookupURL)

			let task = URLSession.shared.dataTask(with: request)
			{ (data, response, error) in

				if error != nil || data == nil
				{	completion(nil)
				}
				else
				{
					do
					{	try completion(JSON(data: data!))
					}
					catch
					{	completion(nil)
					}
				}
			}

			task.resume()
		}
	}

	// Sign a URL with a valid API key to access the Yelp APIs
	class func createRequestFrom(url: URL) -> URLRequest
	{
		var request = URLRequest(url: url)
		request.httpMethod = "GET"
		request.setValue("Bearer \(YELP_API_KEY)", forHTTPHeaderField: "Authorization")

		return request
	}
}
