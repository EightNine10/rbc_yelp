//
//  YelpDataManager.swift
//  RBC Yelp
//
//  Created by Peter Rutherford on 2018-03-10.
//  Copyright © 2018 Peter Rutherford. All rights reserved.
//

import Foundation
import UIKit

// This class manages the data layer for the results return from the Yelp API calls
class YelpDataManager
{
	static var shared = YelpDataManager()

	var restaurantInfoArray = [RestaurantInfo]()
	var restaurantDetails: RestaurantDetails?

	func loadSearchDataFor(term: String, completion: @escaping () -> ())
	{
		restaurantInfoArray.removeAll()

		YelpAPIManager.getSearchResultsFor(term: term)
		{ (json) in

			if let json = json
			{
				for businessJSON in json["businesses"].arrayValue
				{	self.restaurantInfoArray.append(RestaurantInfo(json: businessJSON))
				}
			}

			completion()
		}
	}

	func clearLookupData()
	{
		self.restaurantDetails = nil
	}

	func loadLookupDataFor(businessID: String, completion: @escaping () -> ())
	{
		restaurantInfoArray.removeAll()

		YelpAPIManager.getBusinessLookupFor(businessID: businessID)
		{ (businessJSON) in

			YelpAPIManager.getReviewsFor(businessID: businessID)
			{ (reviewsJSON) in

				if let businessJSON = businessJSON, let reviewsJSON = reviewsJSON
				{	self.restaurantDetails = RestaurantDetails(businessJSON: businessJSON, reviewsJSON: reviewsJSON)
				}

				completion()
			}
		}
	}

	class func createStarImageFrom(rating: Double) -> UIImage?
	{
		let starImageIndex = Int(rating * 2)

		if starImageIndex < starImageNames.count
		{	return UIImage(named: starImageNames[starImageIndex])
		}

		return nil
	}
}

// Class to Store Restaurant Basic Information
class RestaurantInfo
{
	var name: String
	var price: String
	var rating: Double
	var restaurantID: String

	var imageURL: URL?

	init(json: JSON)
	{
		name = json["name"].stringValue
		price = json["price"].stringValue
		rating = json["rating"].doubleValue
		restaurantID = json["id"].stringValue

		imageURL = json["image_url"].url
	}
}

// Class to store Full Restaurant Details
class RestaurantDetails
{
	var name: String
	var price: String
	var rating: Double
	var numReviews: Int

	var imageURL: URL?

	var phoneNumber: String
	var displayAddress = [String]()
	var hours = [RestaurantHour]()

	var photoArray = [URL]()
	var reviews = [RestaurantReview]()

	init(businessJSON: JSON, reviewsJSON: JSON)
	{
		name = businessJSON["name"].stringValue
		price = businessJSON["price"].stringValue
		rating = businessJSON["rating"].doubleValue
		numReviews = businessJSON["review_count"].intValue

		imageURL = businessJSON["image_url"].url

		phoneNumber = businessJSON["phone"].stringValue

		for addressPiece in businessJSON["location"]["display_address"].arrayValue
		{	displayAddress.append(addressPiece.stringValue)
		}

		let hoursArray = businessJSON["hours"].arrayValue

		if hoursArray.count > 0
		{
			for restaurantHourJSON in hoursArray[0]["open"].arrayValue
			{	hours.append(RestaurantHour(json: restaurantHourJSON))
			}
		}

		for photoURL in businessJSON["photos"].arrayValue
		{
			if let photoURL = photoURL.url
			{	photoArray.append(photoURL)
			}
		}

		for reviewJSON in reviewsJSON["reviews"].arrayValue
		{
			reviews.append(RestaurantReview(json: reviewJSON))
		}
	}
}

// Class to store Restaurant Hours of Operation
class RestaurantHour
{
	var day: String

	var startTime: String
	var endTime: String

	init(json: JSON)
	{
		day = RestaurantHour.dayFrom(number: json["day"].intValue)

		startTime = RestaurantHour.timeFrom(string: json["start"].stringValue)
		endTime = RestaurantHour.timeFrom(string: json["end"].stringValue)
	}

	class func dayFrom(number: Int) -> String
	{
		if number == 0 { return "Sunday" }
		if number == 1 { return "Monday" }
		if number == 2 { return "Tuesday" }
		if number == 3 { return "Wednesday" }
		if number == 4 { return "Thursday" }
		if number == 5 { return "Friday" }
		if number == 6 { return "Saturday" }

		return ""
	}

	class func timeFrom(string: String) -> String
	{
		if string.count == 4
		{
			if let hour = Int(string.prefix(2))
			{
				let minuteString = string.suffix(2)
				let meridiem = (hour >= 12) ? "pm" : "am"
				let hourString = (hour == 0) ? "12" : (hour > 12) ? "\(hour - 12)" : "\(hour)"

				return "\(hourString):\(minuteString)\(meridiem)"
			}
		}

		return ""
	}
}

// Class to store Restaurant Review Data
class RestaurantReview
{
	var username: String
	var rating: Double
	var reviewText: String

	var userImageURL: URL?
	var reviewURL: URL?

	var timestamp: String

	init(json: JSON)
	{
		username = json["user"]["name"].stringValue
		rating = json["rating"].doubleValue
		reviewText = json["text"].stringValue

		userImageURL = json["user"]["image_url"].url
		reviewURL = json["url"].url

		let timestampString = json["time_created"].stringValue
		let timestampStringArray = timestampString.components(separatedBy: " ")
		timestamp = timestampStringArray[0]
	}
}
