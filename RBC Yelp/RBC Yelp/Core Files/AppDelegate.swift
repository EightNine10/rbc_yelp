//
//  AppDelegate.swift
//  RBC Yelp
//
//  Created by Peter Rutherford on 2018-03-10.
//  Copyright © 2018 Peter Rutherford. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{
	var window: UIWindow?

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
	{
		return true
	}
}

