//
//  SettingsTableViewController.swift
//  RBC Yelp
//
//  Created by Peter Rutherford on 2018-03-10.
//  Copyright © 2018 Peter Rutherford. All rights reserved.
//

import Foundation
import UIKit

class SettingsTableViewController: UITableViewController
{
	override func numberOfSections(in tableView: UITableView) -> Int
	{
		return 3
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		if section == 0
		{	return SORT_OPTIONS_DISPLAY_ARRAY.count
		}
		else if section == 1
		{	return LIMIT_OPTIONS_ARRAY.count
		}
		else
		{	return PRICE_OPTIONS_ARRAY.count
		}
	}

	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?
	{
		if section == 0
		{	return "SORT BY"
		}
		else if section == 1
		{	return "MAX SEARCH RESULTS"
		}
		else
		{	return "PRICE OPTIONS"
		}
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		if let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell")
		{
			if indexPath.section == 0
			{	cell.textLabel?.text = SORT_OPTIONS_DISPLAY_ARRAY[indexPath.row]
				cell.accessoryType = (indexPath.row == UserDataManager.shared.sortOptionIndex) ? .checkmark : .none
			}
			else if indexPath.section == 1
			{	cell.textLabel?.text = LIMIT_OPTIONS_ARRAY[indexPath.row]
				cell.accessoryType = (indexPath.row == UserDataManager.shared.limitOptionIndex) ? .checkmark : .none
			}
			else if indexPath.section == 2
			{	cell.textLabel?.text = PRICE_OPTIONS_ARRAY[indexPath.row]
				cell.accessoryType = (UserDataManager.shared.priceOptions[indexPath.row]) ? .checkmark : .none
			}

			return cell
		}

		return UITableViewCell()
	}

	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
	{
		if indexPath.section == 0
		{	UserDataManager.shared.sortOptionIndex = indexPath.row
		}
		else if indexPath.section == 1
		{	UserDataManager.shared.limitOptionIndex = indexPath.row
		}
		else if indexPath.section == 2
		{	UserDataManager.shared.priceOptions[indexPath.row] = !UserDataManager.shared.priceOptions[indexPath.row]
		}

		self.tableView.reloadData()
	}

	@IBAction func doneButtonPressed(_ sender: UIBarButtonItem)
	{
		self.dismiss(animated: true, completion: nil)
	}
}
