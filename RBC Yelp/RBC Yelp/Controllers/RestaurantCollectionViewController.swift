//
//  RestaurantCollectionViewController.swift
//  RBC Yelp
//
//  Created by Peter Rutherford on 2018-03-10.
//  Copyright © 2018 Peter Rutherford. All rights reserved.
//

import Foundation
import UIKit

class RestaurantCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UISearchBarDelegate
{
	var searchTerm = DEFAULT_SEARCH_TERM

	var searchController: UISearchController?

	override func viewDidLoad()
	{
		self.collectionView?.refreshControl = UIRefreshControl()
		self.collectionView?.refreshControl?.addTarget(self, action: #selector(RestaurantCollectionViewController.refreshData), for: .valueChanged)

		self.searchController = UISearchController(searchResultsController: nil)
		self.searchController?.searchBar.delegate = self
		self.searchController?.obscuresBackgroundDuringPresentation = false
		self.searchController?.searchBar.placeholder = "Search Restaurants"
		self.navigationItem.searchController = searchController
		self.definesPresentationContext = true
	}

	override func viewWillAppear(_ animated: Bool)
	{
		super.viewWillAppear(animated)
		if #available(iOS 11.0, *)
		{	navigationItem.hidesSearchBarWhenScrolling = false
		}

		self.refreshData()
	}

	override func viewDidAppear(_ animated: Bool)
	{
		super.viewDidAppear(animated)
		if #available(iOS 11.0, *)
		{	navigationItem.hidesSearchBarWhenScrolling = true
		}
	}

	@objc func refreshData()
	{
		YelpDataManager.shared.loadSearchDataFor(term: self.searchTerm)
		{
			DispatchQueue.main.async
			{	self.collectionView?.reloadData()
				self.collectionView?.refreshControl?.endRefreshing()
			}
		}
	}

	func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
	{
		self.searchTerm = searchBar.text ?? self.searchTerm
		self.searchController?.isActive = false
		self.refreshData()
	}

	override func prepare(for segue: UIStoryboardSegue, sender: Any?)
	{
		if let detailViewController = segue.destination as? RestaurantDetailTableViewController
		{
			if let selectedCell = (sender as? RestaurantCollectionViewCell)
			{
				if let indexPath = self.collectionView?.indexPath(for: selectedCell)
				{
					if indexPath.row < YelpDataManager.shared.restaurantInfoArray.count
					{
						YelpDataManager.shared.clearLookupData()

						detailViewController.navigationItem.title = YelpDataManager.shared.restaurantInfoArray[indexPath.row].name
						detailViewController.restaurantID = YelpDataManager.shared.restaurantInfoArray[indexPath.row].restaurantID
					}
				}
			}
		}
	}

	override func numberOfSections(in collectionView: UICollectionView) -> Int
	{
		return 1
	}

	override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
	{
		return YelpDataManager.shared.restaurantInfoArray.count
	}

	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
	{
		// This math is used to ensure good cell sizes
		let screenWidth = UIScreen.main.bounds.width
		let numTilesPerRow = CGFloat(max(2, Int((screenWidth - (GRID_MARGIN * 2)) / GRID_CELL_MAX_WIDTH)))
		let tileWidth = (screenWidth - (GRID_MARGIN * 2) - (GRID_MARGIN * (numTilesPerRow - 1))) / numTilesPerRow

		return CGSize(width: tileWidth, height: tileWidth)
	}

	override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
	{
		if let cell = self.collectionView?.dequeueReusableCell(withReuseIdentifier: "RestaurantCell", for: indexPath) as? RestaurantCollectionViewCell
		{
			cell.setBorder()

			if indexPath.row < YelpDataManager.shared.restaurantInfoArray.count
			{	cell.setData(restaurantInfo: YelpDataManager.shared.restaurantInfoArray[indexPath.row])
			}

			return cell
		}
		else
		{	return UICollectionViewCell()
		}
	}

	override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator)
	{
		DispatchQueue.main.async
		{	self.collectionView?.reloadData()
		}
	}
}
