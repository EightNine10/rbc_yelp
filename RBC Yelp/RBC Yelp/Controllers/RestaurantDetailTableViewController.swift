//
//  RestaurantDetailTableViewController.swift
//  RBC Yelp
//
//  Created by Peter Rutherford on 2018-03-11.
//  Copyright © 2018 Peter Rutherford. All rights reserved.
//

import Foundation
import UIKit

class RestaurantDetailTableViewController: UITableViewController
{
	var restaurantID: String?

	override func viewDidLoad()
	{
		self.refreshData()

		self.tableView.tableFooterView = UIView()
	}

	func refreshData()
	{
		if let businessID = self.restaurantID
		{
			YelpDataManager.shared.loadLookupDataFor(businessID: businessID)
			{
				DispatchQueue.main.async
				{
					self.tableView.reloadData()
				}
			}
		}
	}

	override func prepare(for segue: UIStoryboardSegue, sender: Any?)
	{
		if let destination = segue.destination as? ReviewsWebViewController
		{
			if let selectedCell = (sender as? RestaurantDetailReviewCell)
			{
				if let indexPath = self.tableView?.indexPath(for: selectedCell)
				{
					if let restaurantDetails = YelpDataManager.shared.restaurantDetails
					{
						if indexPath.row < restaurantDetails.reviews.count
						{
							// When the user taps a review, send the URL to the ReviewsWebViewController
							destination.reviewURL = restaurantDetails.reviews[indexPath.row].reviewURL
						}
					}
				}
			}
		}
	}

	override func numberOfSections(in tableView: UITableView) -> Int
	{
		return (YelpDataManager.shared.restaurantDetails == nil) ? 1 : 5
	}

	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
	{
		if section == 2
		{	return YelpDataManager.shared.restaurantDetails?.hours.count ?? 0
		}
		else if section == 3
		{	return 1
		}
		else if section == 4
		{	return YelpDataManager.shared.restaurantDetails?.reviews.count ?? 0
		}

		return 1
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
	{
		if let restaurantDetails = YelpDataManager.shared.restaurantDetails
		{
			if indexPath.section == 0
			{
				if let cell = tableView.dequeueReusableCell(withIdentifier: "DetailTitleCell") as? RestaurantDetailTitleCell
				{
					cell.setRestaurantData(restaurantDetails: restaurantDetails)
					return cell
				}
			}
			else if indexPath.section == 1
			{
				if let cell = tableView.dequeueReusableCell(withIdentifier: "DetailInfoCell") as? RestaurantDetailInfoCell
				{
					cell.setRestaurantData(restaurantDetails: restaurantDetails)
					return cell
				}
			}
			else if indexPath.section == 2
			{
				if let cell = tableView.dequeueReusableCell(withIdentifier: "DetailHourCell")
				{
					if let hours = YelpDataManager.shared.restaurantDetails?.hours
					{
						if indexPath.row < hours.count
						{
							cell.textLabel?.text = hours[indexPath.row].day
							cell.detailTextLabel?.text = hours[indexPath.row].startTime + " - " + hours[indexPath.row].endTime

							return cell
						}
					}
				}
			}
			else if indexPath.section == 3
			{
				if let cell = tableView.dequeueReusableCell(withIdentifier: "DetailPhotosCell") as? RestaurantDetailPhotosCell
				{
					cell.setRestaurantDetails(restaurantDetails: restaurantDetails)
					return cell
				}
			}
			else if indexPath.section == 4
			{
				if let cell = tableView.dequeueReusableCell(withIdentifier: "DetailReviewCell") as? RestaurantDetailReviewCell
				{
					if let reviews = YelpDataManager.shared.restaurantDetails?.reviews
					{
						if indexPath.row < reviews.count
						{
							cell.setRestaurantData(review: reviews[indexPath.row])

							return cell
						}
					}
				}
			}
		}
		else if indexPath.section == 0
		{
			if let cell = tableView.dequeueReusableCell(withIdentifier: "DetailLoadingCell") as? RestaurantDetailLoadingCell
			{
				cell.startLoading()
				return cell
			}
		}

		return UITableViewCell()
	}
}
