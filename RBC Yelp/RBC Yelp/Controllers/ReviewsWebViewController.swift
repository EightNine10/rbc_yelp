//
//  ReviewsWebViewController.swift
//  RBC Yelp
//
//  Created by Peter Rutherford on 2018-03-13.
//  Copyright © 2018 Peter Rutherford. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class ReviewsWebViewController: UIViewController, WKNavigationDelegate
{
	@IBOutlet weak var reviewWebView: WKWebView!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!

	var reviewURL: URL?

	override func viewDidLoad()
	{
		if let reviewURL = self.reviewURL
		{
			self.activityIndicator.isHidden = false
			self.activityIndicator.startAnimating()

			self.reviewWebView.navigationDelegate = self
			self.reviewWebView.load(URLRequest(url: reviewURL))
		}
	}

	func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!)
	{
		self.activityIndicator.stopAnimating()
		self.activityIndicator.isHidden = true
	}
}
