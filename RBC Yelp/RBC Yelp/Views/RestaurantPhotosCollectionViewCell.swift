//
//  RestaurantPhotosCollectionViewCell.swift
//  RBC Yelp
//
//  Created by Peter Rutherford on 2018-03-13.
//  Copyright © 2018 Peter Rutherford. All rights reserved.
//

import Foundation
import UIKit

class RestaurantPhotosCollectionViewCell: UICollectionViewCell
{
	@IBOutlet weak var photoImageView: WebImageView!
}
