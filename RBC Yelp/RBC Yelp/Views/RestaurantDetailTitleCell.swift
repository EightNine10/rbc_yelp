//
//  RestaurantDetailTitleCell.swift
//  RBC Yelp
//
//  Created by Peter Rutherford on 2018-03-12.
//  Copyright © 2018 Peter Rutherford. All rights reserved.
//

import Foundation
import UIKit

class RestaurantDetailTitleCell: UITableViewCell
{
	@IBOutlet weak var titleImageView: WebImageView!
	@IBOutlet weak var titlePriceLabel: UILabel!
	@IBOutlet weak var titleNameLabel: UILabel!
	@IBOutlet weak var titleStarImageView: UIImageView!

	func setRestaurantData(restaurantDetails: RestaurantDetails)
	{
		titleNameLabel.text = restaurantDetails.name
		titlePriceLabel.text = restaurantDetails.price

		if let image = YelpDataManager.createStarImageFrom(rating: restaurantDetails.rating)
		{	titleStarImageView.image = image
		}

		if let imageURL = restaurantDetails.imageURL
		{	titleImageView.loadImageFrom(url: imageURL)
		}

	}
}
