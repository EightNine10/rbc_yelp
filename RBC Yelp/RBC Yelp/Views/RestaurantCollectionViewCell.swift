//
//  RestaurantCollectionViewCell.swift
//  RBC Yelp
//
//  Created by Peter Rutherford on 2018-03-10.
//  Copyright © 2018 Peter Rutherford. All rights reserved.
//

import Foundation
import UIKit

class RestaurantCollectionViewCell: UICollectionViewCell
{
	@IBOutlet weak var restaurantNameLabel: UILabel!
	@IBOutlet weak var restaurantPriceLabel: UILabel!
	@IBOutlet weak var restaurantStarImageView: UIImageView!

	@IBOutlet weak var restaurantImageView: WebImageView!

	required init?(coder aDecoder: NSCoder)
	{
		super.init(coder: aDecoder)
	}

	func setData(restaurantInfo: RestaurantInfo)
	{
		restaurantNameLabel.text = restaurantInfo.name
		restaurantPriceLabel.text = restaurantInfo.price

		if let image = YelpDataManager.createStarImageFrom(rating: restaurantInfo.rating)
		{	restaurantStarImageView.image = image
		}

		if let imageURL = restaurantInfo.imageURL
		{	restaurantImageView.loadImageFrom(url: imageURL)
		}
	}

	func setBorder()
	{
		// Modify the layer of the cell to create a 'card' effect
		self.contentView.layer.cornerRadius = 0.0
		self.contentView.layer.borderWidth = 1.0
		self.contentView.layer.borderColor = UIColor(displayP3Red: 0.8, green: 0.8, blue: 0.8, alpha: 1.0).cgColor
		self.contentView.layer.masksToBounds = true

		self.layer.shadowColor = UIColor.lightGray.cgColor
		self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
		self.layer.shadowRadius = 2.0
		self.layer.shadowOpacity = 1.0
		self.layer.masksToBounds = false
		self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
	}
}
