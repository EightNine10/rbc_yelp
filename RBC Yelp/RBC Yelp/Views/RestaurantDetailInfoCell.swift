//
//  RestaurantDetailInfoCell.swift
//  RBC Yelp
//
//  Created by Peter Rutherford on 2018-03-12.
//  Copyright © 2018 Peter Rutherford. All rights reserved.
//

import Foundation
import UIKit

class RestaurantDetailInfoCell: UITableViewCell
{
	@IBOutlet weak var infoLocationLabel: UILabel!
	@IBOutlet weak var infoPhoneLabel: UILabel!

	func setRestaurantData(restaurantDetails: RestaurantDetails)
	{
		infoLocationLabel.text = ""

		for displayPiece in restaurantDetails.displayAddress
		{
			if let text = infoLocationLabel.text
			{
				if displayPiece == restaurantDetails.displayAddress.last
				{	infoLocationLabel.text = text + displayPiece
				}
				else
				{	infoLocationLabel.text = text + displayPiece + "\n"
				}
			}


		}

		infoPhoneLabel.text = "Phone: " + restaurantDetails.phoneNumber
	}
}

