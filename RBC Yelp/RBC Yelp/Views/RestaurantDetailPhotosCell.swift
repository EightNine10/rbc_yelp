//
//  RestaurantDetailPhotosCell.swift
//  RBC Yelp
//
//  Created by Peter Rutherford on 2018-03-13.
//  Copyright © 2018 Peter Rutherford. All rights reserved.
//

import Foundation
import UIKit

class RestaurantDetailPhotosCell: UITableViewCell, UICollectionViewDataSource
{
	@IBOutlet weak var photosCollectionView: UICollectionView!

	var photos = [URL]()

	func setRestaurantDetails(restaurantDetails: RestaurantDetails)
	{
		self.photosCollectionView.dataSource = self

		self.photos = [URL]()

		for photo in restaurantDetails.photoArray
		{	self.photos.append(photo)
		}

		self.photosCollectionView.reloadData()
	}

	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
	{
		return self.photos.count
	}

	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
	{
		if let cell = self.photosCollectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as? RestaurantPhotosCollectionViewCell
		{
			cell.photoImageView.loadImageFrom(url: self.photos[indexPath.row])
			return cell
		}

		return UICollectionViewCell()
	}


}
