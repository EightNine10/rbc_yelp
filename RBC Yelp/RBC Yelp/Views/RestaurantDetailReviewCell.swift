//
//  RestaurantDetailReviewCell.swift
//  RBC Yelp
//
//  Created by Peter Rutherford on 2018-03-13.
//  Copyright © 2018 Peter Rutherford. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class RestaurantDetailReviewCell: UITableViewCell
{
	@IBOutlet weak var usernameLabel: UILabel!
	@IBOutlet weak var reviewLabel: UILabel!
	@IBOutlet weak var timestampLabel: UILabel!

	@IBOutlet weak var userImageView: WebImageView!
	@IBOutlet weak var starRatingImageView: UIImageView!

	var reviewURL: URL?

	func setRestaurantData(review: RestaurantReview)
	{
		usernameLabel.text = review.username
		reviewLabel.text = review.reviewText
		timestampLabel.text = review.timestamp

		if let userImageURL = review.userImageURL
		{	userImageView.loadImageFrom(url: userImageURL)
		}

		if let image = YelpDataManager.createStarImageFrom(rating: review.rating)
		{	starRatingImageView.image = image
		}

		self.reviewURL = review.reviewURL
	}
}
